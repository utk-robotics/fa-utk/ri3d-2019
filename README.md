# Big Orange Robotics Ri3D Team 2019

Written in python with robotpy

https://bigorangerobots.com/

## Dependencies

Python 3

robotpy [installation: https://robotpy.readthedocs.io/en/stable/install/robot.html#install-robotpy](https://robotpy.readthedocs.io/en/stable/install/robot.html#install-robotpy)

#### pip packages

 * pyfrc
 * robotpy-cscore
 * wpilib

#### packages for roborio

Use `download-opkg` and `install-opkg` for items in `robot-opkgs.md`.

## Deploying to RoboRIO

```bash
python3 robot.py deploy --skip-tests
```
