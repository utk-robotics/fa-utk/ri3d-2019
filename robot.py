#!/usr/bin/env python3
"""
    This is a good foundation to build your robot code on
"""
from cscore import CameraServer
# from cscore import VideoSink
import wpilib
import wpilib.drive
from wpilib.interfaces import GenericHID
Hand = GenericHID.Hand
from networktables import NetworkTables

class MyRobot(wpilib.TimedRobot):
    def robotInit(self):
        """
        This function is called upon program startup and
        should be used for any initialization code.

        DRIVETRAIN:
        4 SPARK MAX (PWM)
        """
        self.hatcher_toggle = True
        self.front_left_motor = wpilib.Spark(0)
        self.front_left_motor.setInverted(True)
        self.back_left_motor = wpilib.Spark(1)
        self.back_left_motor.setInverted(True)
        self.front_right_motor = wpilib.Spark(2)
        self.front_right_motor.setInverted(True)
        self.back_right_motor = wpilib.Spark(3)
        self.back_right_motor.setInverted(True)
        self.direction = 1

        # 3 Other sparks (uncategorized)
        self.shooter_turner = wpilib.Spark(4)
        self.front_climber = wpilib.Spark(5)
        self.back_climber = wpilib.Spark(6)

        # 7-9 other talons
        self.left_shooter = wpilib.Talon(8)
        self.right_shooter = wpilib.Talon(9)
        self.back_climb_wheel = wpilib.Talon(7)

        # 3 talons (ids need to be set)
        self.left_motor_group = wpilib.SpeedControllerGroup(self.front_left_motor, self.back_left_motor)
        self.right_motor_group = wpilib.SpeedControllerGroup(self.front_right_motor, self.back_right_motor)

        # Solenoid compressor
        self.compressor = wpilib.Compressor(0)

        # pneumatic solenoid
        self.solenoid_hatcher = wpilib.Solenoid(0)
        self.solenoid_shooter = wpilib.Solenoid(1)

        print('xd1!!!!')

        # Camera
        self.camera_server = CameraServer.getInstance()
        self.toggle = False
        self.camera_server.enableLogging()

        self.usb1 = self.camera_server.startAutomaticCapture(dev=0)
        #self.usb2 = self.camera_server.startAutomaticCapture(dev=1)

        self.camera_server.kBasePort = 1180

        self.usb1.setResolution(1280, 720)
        #self.usb2.setResolution(1280, 720)

        self.drive = wpilib.drive.DifferentialDrive(self.left_motor_group, self.right_motor_group)
        self.timer = wpilib.Timer()
        self.time_start = self.timer.getMsClock()

        # Buttons
        self.primary_controller = wpilib.XboxController(0)
        self.primary_joystick = wpilib.Joystick(0)
        # self.right_stick = wpilib.Joystick(1)

        # Rip auto
        self.autonomousInit = self.teleopInit
        self.autonomousPeriodic = self.teleopPeriodic

    def teleopInit(self):
        self.compressor.setClosedLoopControl(True)

    def teleopPeriodic(self):
        """This function is called periodically during operator control."""
        self.drive.arcadeDrive(
            self.primary_controller.getY(Hand.kLeft) / 2.0, # * self.direction,
            -self.primary_controller.getX(Hand.kLeft) / 2.0 # * self.direction
        )

        # two front analog triggers to shooter wheels
        if self.primary_controller.getTriggerAxis(Hand.kLeft) != 0.0:
            self.right_shooter.set(self.primary_controller.getTriggerAxis(Hand.kLeft))
            self.left_shooter.set(-self.primary_controller.getTriggerAxis(Hand.kLeft))

        elif self.primary_controller.getTriggerAxis(Hand.kRight) != 0.0:
            self.right_shooter.set(-self.primary_controller.getTriggerAxis(Hand.kRight))
            self.left_shooter.set(self.primary_controller.getTriggerAxis(Hand.kRight))

        else:
            self.right_shooter.set(0)
            self.left_shooter.set(0)

        # right bumper: shooter pneumatic
        if self.primary_controller.getBumper(Hand.kRight):
            self.solenoid_shooter.set(True)
        else:
            self.solenoid_shooter.set(False)

        # X toggles hatcher
        if self.primary_controller.getXButtonPressed():
            self.solenoid_hatcher.set(self.hatcher_toggle)
            self.hatcher_toggle = not self.hatcher_toggle

        # D pad up: raise front climbers up
        pov = self.primary_joystick.getPOV()
        # print("POV: " + str(pov))
        if pov == -1:
            self.front_climber.set(0)
            self.back_climber.set(0)
        elif pov == 0:
            self.front_climber.set(.6)
            self.back_climber.set(0)
        elif pov == 45:
            self.front_climber.set(.6)
            self.back_climber.set(.6)
        # D pad right: back climber down
        elif pov == 90:
            self.front_climber.set(0)
            self.back_climber.set(-.6)
        elif pov == 135:
            self.front_climber.set(-.6)
            self.back_climber.set(-.6)
        # D pad down: front climber down
        elif pov == 180:
            self.front_climber.set(-.6)
            self.back_climber.set(0)
        elif pov == 225:
            self.front_climber.set(-.6)
            self.back_climber.set(.6)
        # D pad left: back climber up
        elif pov == 270:
            self.front_climber.set(0)
            self.back_climber.set(.6)
        elif pov == 315:
            self.front_climber.set(0.6)
            self.back_climber.set(0.6)

        # Climber wheel
        self.back_climb_wheel.set(self.primary_controller.getY(Hand.kRight))

        # Shooter tilt
        self.shooter_turner.set(self.primary_controller.getX(Hand.kRight) / 4.0)

    def robotPeriodic(self):

        if self.primary_controller.getYButtonPressed():
            if self.toggle: # Forward (TODO: configure path)
                print('switched to cam 2')
                self.direction = 1
                self.camera_server.removeCamera(self.usb1)
                self.camera_server.addCamera(self.usb2)

            else: # back TODO: configure path
                print('switched to cam 1')
                self.direction = -1
                self.camera_server.removeCamera(self.usb2)
                self.camera_server.addCamera(self.usb1)

            self.toggle = not self.toggle

if __name__ == "__main__":
    wpilib.run(MyRobot)
